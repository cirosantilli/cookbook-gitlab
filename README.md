Hack of [GitLab Cookbook](https://gitlab.com/gitlab-org/cookbook-gitlab) intended for development and production installations of [GitLab Elearn](https://github.com/cirosantilli/gitlab-elearn).

Should be used to install GitLab Elearn instead of the original GitLab cookbook.

All original Cookbook instructions apply unless stated otherwise.

Best development install: [metal development install](doc/development_metal.md).

# Merge back into GitLab

Our goal is to merge back as much as possible into GitLab, so when you want to start a new feature for GitLab just:

	# Must keep `origin` for GitLab upstream
	git remote rename origin mine
	git remote add origin https://github.com/gitlabhq/gitlabhq
	git fetch origin
	# `up` for upstream
	git checkout -b up origin/master
	bundle install
	bundle exec rake db:schema:load
	bundle exec rake db:seed_fu

# AWS production install

Always use `RAILS_ENV=production`: it is not on by default. Consider adding it to your `.bashrc`:

    echo 'export RAILS_ENV=production' >> ~/.bashrc

Do not forget to `rake assets:precompile` when you change an asset.

Consider stopping the server before operations such as `rake gitlab:setup` or `rake assets:precompile`.

If you do a free tier production install on AWS first allocate some swap space with:

    sudo dd if=/dev/zero of=/swapfile bs=1024 count=1024k
    sudo mkswap /swapfile
    sudo swapon /swapfile
    sudo bash -c 'echo "/swapfile       none    swap    sw      0       0" >> /etc/fstab'

or else GitLab will choke on the measly 0.5Gb you have.
